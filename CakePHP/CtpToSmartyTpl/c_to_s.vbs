'/**
' * CakePHPのbakeで生成されたctpファイルをSmartyテンプレートファイル(tpl)に変換
' * ほぼ使える状態で生成できる
' * 変換後のtplの加工が一部必要
' *
' * コマンドラインからの呼び出しにも対応
' *
' * 呼び出し例）
' * > cscript c_to_s.vbs C:\develop\sample1\src\Template\Users\index.ctp
' *
' */

Dim srcPath
Dim distPath
Dim fso
Dim srcFile
Dim distFile
Dim objRegExp
Dim isHeadComment
Dim objArgs

Set objArgs = WScript.Arguments

If objArgs.Count > 0 Then
  srcPath = objArgs(0)
Else
  srcPath = InputBox("Please input CTP full path or relative path", "c_to_s")
End If

If srcPath = "" Then
  MsgBox "Input is empty!"
  WScript.Quit
End If

Set objRegExp = New RegExp
objRegExp.Pattern = """(.*)"""
srcPath = objRegExp.Replace(CStr(srcPath), "$1")

Set fso = CreateObject("Scripting.FileSystemObject")

If fso.FileExists(srcPath) = Flase Then
  WScript.Echo "File Not Found! : [" + srcPath + "]"
  WScript.Quit
End If

distPath = Replace(srcPath, ".ctp", ".tpl")
Set srcFile = fso.OpenTextFile(srcPath)
Set distFile = fso.CreateTextFile(distPath)

isHeadComment = true

With srcFile
  Do Until .AtEndOfStream
    tmpTxt = .ReadLine

    If isHeadComment Then
      If tmpTxt = "?>" Then
        isHeadComment = false
      End If
    Else
      tmpTxt = Replace(tmpTxt, "<?php ", "{")
      tmpTxt = Replace(tmpTxt, "<?php", "{")
      tmpTxt = Replace(tmpTxt, "<?= ", "{")
      tmpTxt = Replace(tmpTxt, " ?>", "}")
      tmpTxt = Replace(tmpTxt, ":}", "}")
      tmpTxt = Replace(tmpTxt, "endif;", "/if")
      tmpTxt = Replace(tmpTxt, "endforeach;", "/foreach")
      Set objRegExp = New RegExp
      objRegExp.Pattern = "foreach \((.*)\)"
      tmpTxt = objRegExp.Replace(tmpTxt, "foreach $1")

      Set objRegExp = New RegExp
      objRegExp.Pattern = "echo (.*);"
      tmpTxt = objRegExp.Replace(tmpTxt, "{$1}")

      Set objRegExp = New RegExp
      objRegExp.Pattern = "h\((.*?)\)"
      tmpTxt = objRegExp.Replace(tmpTxt, "$1|escape")

      tmpTxt = Replace(tmpTxt, ";}", "}")
      distFile.WriteLine(tmpTxt)
    End If
  Loop

  .Close
End With

distFile.Close
